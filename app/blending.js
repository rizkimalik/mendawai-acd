'use strict'
const knex = require('./connection');
const date = require('date-and-time');
const logger = require('../helper/logger');
const { onSendMsgAgent, onSendMsgCustomer, onQueingChat, onSendMessageWhatsapp, onSendTwitterDirectMessage, onSendEmailIn, onSendFacebookMessenger, onSendInstagramMessenger, onSendMessageTelegram, onSendInstagramFeed, onSendFacebookFeed } = require('./socket');

const blending_chat = async function () {
    try {
        //? check count total
        const { total } = await knex('chats').count('chat_id as total').where({ agent_handle: null, status_chat: 'waiting', flag_to: 'customer', flag_end: 'N', channel: 'Chat' }).first();
        if (total > 0) {
            //? get data chat
            const chat = await knex('chats').select('id', 'chat_id', 'user_id', 'email', 'name', 'customer_id', 'message', 'channel', 'status_chat', 'agent_handle', 'flag_to', 'date_create')
                .where({
                    agent_handle: null,
                    status_chat: 'waiting',
                    flag_to: 'customer',
                    flag_end: 'N',
                    channel: 'Chat'
                })
                .orderBy('id', 'asc').first();


            //? get data user/agent on ready
            const check_acd = await knex.raw(`select * from v_bucket_agent_chat WHERE total_handle < max_chat ORDER BY last_distribute_chat ASC LIMIT 1`);
            const user = check_acd[0][0];

            //? validation data 
            if (chat && user) {
                //? get data customer ready
                const customer = await knex('customers').select('name', 'uuid')
                    .where({
                        email: chat.email,
                        customer_id: chat.customer_id,
                    }).first();

                //? assign agent_handle
                await knex('chats')
                    .update({ agent_handle: user.username, date_assign: knex.fn.now() })
                    .where({ chat_id: chat.chat_id })

                //? assig date to users
                await knex('users')
                    .update({ last_distribute_chat: knex.fn.now() })
                    .where({ username: user.username })

                logger('acd/chat', `${chat.email} = has assign to agent: ${user.username}`);


                //?send to socket
                await onSendMsgCustomer({ customer, user, chat });
                setTimeout(async () => {
                    await onSendMsgAgent({ customer, user, chat });
                }, 3000);
            }
            else {
                logger('acd/chat', `[Chat]: agent chat login not available.`);

                if (chat) {
                    logger('acd/chat', `[Chat]: queue ${total}, customer ready to chat.`);

                    const chat_queing = await knex('chats')
                        .select('id', 'chat_id', 'email', 'name', 'customer_id', 'message', 'status_chat', 'agent_handle', 'flag_to', 'date_create')
                        .where({
                            agent_handle: null,
                            status_chat: 'waiting',
                            flag_to: 'customer',
                            flag_end: 'N',
                            channel: 'Chat'
                        })
                        .orderBy('id', 'asc');

                    for (let i = 0; i < chat_queing.length; i++) {
                        const data_customer = await knex('customers').select('name', 'uuid')
                            .where({
                                email: chat_queing[i].email,
                                customer_id: chat_queing[i].customer_id,
                            }).first();
                        const data_chat = chat_queing[i];
                        data_chat.queing = i + 1;
                        data_chat.total_queing = total;
                        await onQueingChat({ data_customer, data_chat });
                    }
                }
            }
        }
        else {
            logger('acd/chat', `[Chat]: queue ${total}, all customer already distribute.`);
        }
    }
    catch (error) {
        logger('acd/chat', error.message);
    }
}

const blending_email = async function () {
    try {
        const sqlBlendingEmail = await knex.raw(`CALL sp_auto_distribute_email()`);
        const result = sqlBlendingEmail[0][0][0];
        if (result.status === 'assigned') {
            console.log(date.format(new Date, 'Y-MM-DD H:m:s') + ' [Email]: ' + result.message);
            logger('acd/email', JSON.stringify(result));

            const email = await knex('email_mailbox').where({ email_id: result.email_id }).select('email_id', 'efrom', 'esubject', 'agent_handle', 'date_email', 'date_receive').first();
            await onSendEmailIn({ email });
        }
        else if (result.status === 'queing') {
            logger('acd/email', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/email', result.message);
        }
        else {
            logger('acd/email', JSON.stringify(sqlBlendingEmail));
        }
    } catch (error) {
        logger('acd/email', error.message);
    }
}

const blending_whatsapp = async function () {
    try {
        const sqlBlendingWhatsapp = await knex.raw(`call sp_auto_distribute_whatsapp()`);
        const result = sqlBlendingWhatsapp[0][0][0];

        if (result.status === 'assigned') {
            console.log(date.format(new Date, 'Y-MM-DD H:m:s') + ' [Whatsapp]: ' + result.message);
            logger('acd/whatsapp', JSON.stringify(result));

            //? get data customer,user,chat
            const customer = await knex('customers').select('name', 'uuid').where({ phone_number: result.phone_number, customer_id: result.customer_id }).first();
            const user = await knex('users').select('username', 'uuid').where({ username: result.agent_handle }).first();
            const chat = await knex('chats')
                .where({ channel: 'Whatsapp', chat_id: result.chat_id, agent_handle: result.agent_handle, user_id: result.phone_number })
                .orWhere({ channel: 'Whatsapp_Group' })
                .orWhere({ channel: 'Whatsapp_Business' })
                .orderBy('id', 'desc').first();

            if (chat.message_type !== 'text') {
                let attachment = await knex('chat_attachments').where({ chat_id: chat.chat_id, message_id: chat.message_id })
                chat.attachment = attachment ? attachment : '';
            }
            await onSendMessageWhatsapp({ customer, user, chat });
        }
        else if (result.status === 'queing') {
            logger('acd/whatsapp', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/whatsapp', result.message);
        }
        else {
            logger('acd/whatsapp', JSON.stringify(sqlBlendingWhatsapp));
        }
    } catch (error) {
        logger('acd/whatsapp', error.message);
    }
}

const blending_twitter = async function () {
    try {
        const sqlBlendingTwitter = await knex.raw(`call sp_auto_distribute_twitter()`);
        const result = sqlBlendingTwitter[0][0][0];

        if (result.status === 'assigned') {
            logger('acd/blending_twitter', JSON.stringify(result.message));

            //? get data customer,user,chat
            const customer = await knex('customers').select('name', 'uuid').where({ account_id: result.account_id, customer_id: result.customer_id }).first();
            const user = await knex('users').select('username', 'uuid').where({ username: result.agent_handle }).first();
            const chat = await knex('chats')
                .where({ chat_id: result.chat_id, agent_handle: result.agent_handle, user_id: result.account_id })
                .orderBy('id', 'desc').first();

            if (chat.message_type !== 'text') {
                let attachment = await knex('chat_attachments').where({ chat_id: chat.chat_id, message_id: chat.message_id })
                chat.attachment = attachment ? attachment : '';
            }

            if (chat.channel === 'Twitter_DM') {
                await onSendTwitterDirectMessage({ customer, user, chat });
            }
            else if (chat.channel === 'Twitter_Mention') {
                // await onSendTwitterDirectMessage({ customer, user, chat });
            }
        }
        else if (result.status === 'queing') {
            logger('acd/blending_twitter', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/blending_twitter', result.message);
        }
        else {
            logger('acd/blending_twitter', JSON.stringify(sqlBlendingTwitter));
        }
    } catch (error) {
        logger('acd/blending_twitter', error.message);
    }
}

const blending_facebook = async function () {
    try {
        const sqlBlendingFb = await knex.raw(`call sp_auto_distribute_facebook()`);
        const result = sqlBlendingFb[0][0][0];

        if (result.status === 'assigned') {
            logger('acd/blending_facebook', JSON.stringify(result));

            //? get data customer,user,chat
            const customer = await knex('customers').select('name', 'uuid').where({ account_id: result.account_id, customer_id: result.customer_id }).first();
            const user = await knex('users').select('username', 'uuid').where({ username: result.agent_handle }).first();
            const chat = await knex('chats')
                .where({ chat_id: result.chat_id, agent_handle: result.agent_handle, user_id: result.account_id })
                .orderBy('id', 'desc').first();

            if (chat.message_type !== 'text') {
                let attachment = await knex('chat_attachments').where({ chat_id: chat.chat_id, message_id: chat.message_id })
                chat.attachment = attachment ? attachment : '';
            }

            if (chat.channel === 'Facebook_Messenger') {
                await onSendFacebookMessenger({ customer, user, chat });
            }
            else if (chat.channel === 'Facebook_Feed') {
                await onSendFacebookFeed({ customer, user, chat });
            }
        }
        else if (result.status === 'queing') {
            logger('acd/blending_facebook', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/blending_facebook', result);
        }
        else {
            logger('acd/blending_facebook', JSON.stringify(sqlBlendingFb));
        }
    } catch (error) {
        logger('acd/blending_facebook', error.message);
    }
}

const blending_instagram = async function () {
    try {
        const sqlBlendingInstagram = await knex.raw(`call sp_auto_distribute_instagram()`);
        const result = sqlBlendingInstagram[0][0][0];

        if (result.status === 'assigned') {
            logger('acd/blending_instagram', JSON.stringify(result));

            //? get data customer,user,chat
            const customer = await knex('customers').select('name', 'uuid').where({ account_id: result.account_id, customer_id: result.customer_id }).first();
            const user = await knex('users').select('username', 'uuid').where({ username: result.agent_handle }).first();
            const chat = await knex('chats')
                .where({ chat_id: result.chat_id, agent_handle: result.agent_handle, user_id: result.account_id })
                .orderBy('id', 'desc').first();

            if (chat.message_type !== 'text') {
                let attachment = await knex('chat_attachments').where({ chat_id: chat.chat_id, message_id: chat.message_id })
                chat.attachment = attachment ? attachment : '';
            }

            if (chat.channel === 'Instagram_Messenger') {
                await onSendInstagramMessenger({ customer, user, chat });
            }
            else if (chat.channel === 'Instagram_Feed') {
                await onSendInstagramFeed({ customer, user, chat });
            }
        }
        else if (result.status === 'queing') {
            logger('acd/blending_instagram', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/blending_instagram', result.message);
        }
        else {
            logger('acd/blending_instagram', JSON.stringify(sqlBlendingInstagram));
        }
    } catch (error) {
        logger('ERROR/acd/blending_instagram', error.message);
    }
}

const blending_telegram = async function () {
    try {
        const sqlBlendingTelegram = await knex.raw(`call sp_auto_distribute_telegram()`);
        const result = sqlBlendingTelegram[0][0][0];

        if (result.status === 'assigned') {
            logger('acd/telegram_message', JSON.stringify(result));

            //? get data customer,user,chat
            const customer = await knex('customers').select('name', 'uuid').where({ account_id: result.account_id, customer_id: result.customer_id }).first();
            const user = await knex('users').select('username', 'uuid').where({ username: result.agent_handle }).first();
            const chat = await knex('chats')
                .where({ channel: 'Telegram', chat_id: result.chat_id, agent_handle: result.agent_handle, user_id: result.account_id })
                .orderBy('id', 'desc').first();

            if (chat.message_type !== 'text') {
                let attachment = await knex('chat_attachments').where({ chat_id: chat.chat_id, message_id: chat.message_id })
                chat.attachment = attachment ? attachment : '';
            }
            await onSendMessageTelegram({ customer, user, chat });
        }
        else if (result.status === 'queing') {
            logger('acd/telegram_message', result.message);
        }
        else if (result.status === 'distributed') {
            logger('acd/telegram_message', result.message);
        }
        else {
            logger('acd/telegram_message', JSON.stringify(sqlBlendingTelegram));
        }
    } catch (error) {
        logger('acd/telegram_message', error.message);
    }
}

const blending_call = async function () {

}

module.exports = {
    blending_chat,
    blending_email,
    blending_call,
    blending_whatsapp,
    blending_twitter,
    blending_facebook,
    blending_instagram,
    blending_telegram,
}